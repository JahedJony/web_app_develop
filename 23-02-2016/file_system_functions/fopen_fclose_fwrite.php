<?php

	$filename="file2.txt";
	$somecontent="Add this to the file";

	if (is_writable($filename)){
		if(!$handle=fopen($filename,'a')){
			echo "Can not open file ($filename)";
			exit;
		}
		
		if(fwrite($handle,$somecontent)==FALSE){
			echo "Can not write to the file ($filename)";
			exit;
		}
		
		echo "Success, Wrote ($somecontent) to file ($filename)";
		
		fclose($handle);
	}else{
		echo "This file $filename is not writable";
	}
?>