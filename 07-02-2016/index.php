<?php
echo 'This is a test'; //This is a one-line c++ style comment
echo '<br/>';
/* This is a multi line comment
yet another line of comment */
echo 'This is yet another test';
echo '<br/>';
echo 'One final Test'; #this is a one-line shell-style comment
echo '<br/>';
?>

<?php
$foo = True;
?>

<?php
echo $a = 1234; //decimal number
echo '<br/>';
echo $a = -123; //a negative number
echo '<br/>';
echo $a = 0123; //octal number (equivalant to 83 decimal)
echo '<br/>';
echo $a = 0x1A; //hexadecimal number (equivalant to 26 decimal)
echo '<br/>';
echo $a = 0b11111111; //binary number (equivalant to 255 decimal)
echo '<br/>';
?>


<?php
echo $a = 1.234;
echo '<br/>';
echo $b = 1.2e3;
echo '<br/>';
?>
<?php
    
echo 'this is a simple string<br>';
    
echo 'You can also have embedded newlines in
strings this way as it is okay to do';
echo '<br/>';
    
// outputs: Arnold once said: "I'll be back"
echo 'Android once said: "I\'ll be back"';
echo '<br/>';
    
// outputs: you deleted c:\*.*?
echo 'You deleted C:\\*.*?';
echo '<br/>';

echo 'Variables do not $expand $either';
echo '<br/>';

?>


<?php
echo "this is a double quoted string";
echo '<br/>';
?>

<?php
$str = <<<EOD
Example of String
spanning multiple lines
using heredoc syntax.
EOD;

echo $str;
echo '<br/>';
?>


<?php
$str = <<<EOD
Example of String
spanning multiple lines
using nowdoc syntax.
EOD;

echo $str;
echo '<br/>';
?>

<?php
$cars = array("Volvo", "BMW", "Toyota");
echo "I like ".$cars[0].", ".$cars[1]." and ".$cars[2].'.';
echo '<br/>';
?>

<?php
$var = 'Bob';
$Var = 'Joe';
echo "$var, $Var";
echo '<br/>';

$_4site = 'not yet';
echo $_4site;
echo '<br/>';
$tayte = 'maniskka';
echo $tayte;
echo '<br/>';
?>

<?php
$x = 75;
$y = 25;

function addition(){
    $GLOBALS['z'] = $GLOBALS['x']+$GLOBALS['y'];
}
addition();
echo $z;
echo '<br/>';
?>


<?php
echo $_SERVER['PHP_SELF'];
echo '<br/>';
echo $_SERVER['SERVER_NAME'];
echo '<br/>';
echo $_SERVER['HTTP_HOST'];
echo '<br/>';
echo $_SERVER['HTTP_USER_AGENT'];
echo '<br/>';
echo $_SERVER['SCRIPT_NAME'];
echo '<br/>';
?>



<?php
$var = '122.34343The';
$float_value_of_var = floatval($var);
echo $float_value_of_var;
echo '<br/>';
?>


<?php
$var = 0;
echo empty($var);
echo '<br/>';
?>

<?php
$yes = array('this', 'is', 'an array');
echo is_array($yes);
echo '<br/>';
?>


<?php
$foo = NULL;
var_dump(is_null($foo));
echo '<br/>';
?>

<?php
$var ='';
if (isset($var)){
    echo "This is set so I will print";
}

$a = "test";
$b = "anothertest";

var_dump(isset($a));
var_dump(isset($a, $b));
echo '<br/>';

unset($a);

var_dump(isset($a));
var_dump(isset($a, $b));
echo '<br/>';

$foo = NULL;
var_dump(isset($foo));
echo '<br/>';
?>


<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r($a);
echo '<br/>';
?>



<?php
$serialized_data = serialize(array('Math', 'Language', 'Science'));
echo $serialized_data;
echo '<br/>';
?>


<?php
$serialized_data = serialize(array('Math', 'Language', 'Science'));
echo $serialized_data. '<br/>';

$var1 = unserialize($serialized_data);
var_dump($var1);

echo '<br/>';
?>


<?php
$a = 10;
unset ($a);
$a = 5;
echo $a;
echo '<br/>';
?>


<?php
$a = array(1,2,array("a", "b", "c"));
var_dump($a);
echo '<br/>';
?>