<?php
    // single quoted string
    echo 'this is a simple string<br>';
    
    echo 'You can also have embedded newlines in
    strings this way as it is okay to do';
    echo '</br>';
    
    // outputs: Arnold once said: "I'll be back"
    echo 'Android once said: "I\'ll be back"';
    echo '</br>';
    
    // outputs: you deleted c:\*.*?
    echo 'You deleted C:\\*.*?';
    echo '</br>';

    echo 'Variables do not $expand $either';
    echo '</br>';
    
    // double quoted string 
    echo "this is a double quoted string";
    echo '</br>';

?>
