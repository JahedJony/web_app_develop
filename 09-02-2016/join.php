<?php
    //first example
    $array = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10');    
    echo join('<br>', $array),"</br>";
    
    //first example (result)
    /*
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    */
    
    //second example
    $arr[] = '1a';
    $arr[] = '2b';
    $arr[] = '3c';
    $arr[] = '4d';
    $arr[] = '5e';
    $arr[] = '6f';
    $arr[] = '7g';
    $arr[] = '8h';
    $arr[] = '9i';
    $arr[] = '10j';
    
    echo join('<br>', $arr);
    //second example (result)
    /*
    1a
    2b
    3c
    4d
    5e
    6f
    7g
    8h
    9i
    10j
    */